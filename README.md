
## 「公司金融」 课程主页 - 本科 

&emsp;


### 1. 课程概览

----
- **任课教师：** 邓家品，dengjp5@mail.sysu.edu.cn, Tel：020-84110673
- **助教：** 邢子煜 博士 (xingzy5@mail2.sysu.edu.cn)
- **成绩构成：** 
  - 课堂参与和考勤 10%；课堂参与（小组案例报告）10%；个人作业 30%；期末考试 50%
- **课程内容和进度**：以教务系统公布版本为准。本文底部可以快速浏览。
- **教材：** 乔纳森•伯克、彼得•德马佐著，《公司理财》（上下册，第三版） 姜英兵译，中国人民大学出版社，ISBN：9787300196312, ([上](https://quqi.gblhgk.com/s/880197/1YUQk8Hr9iseBcJ5)，[下](https://quqi.gblhgk.com/s/880197/yAFra4o9dPfSjuzO))，可点击下载电子版。
- **课件：** 公司金融课件合集链接：https://pan.baidu.com/s/1qjqJ6Iog_ejPe4XLOGUEFg 提取码：询问任课老师

&emsp;

### 2. 作业习题 （个人独立完成）

#### $\color{red}{注意作业提交的截止时间！}$ 
---
> 请前往 [「公司金融作业」](https://gitee.com/Dengjp/CF/wikis/Home) 页面查看。
> - **HW01：** 截止时间：2020年9月16日
> - **HW02：** 截止时间：视教学进度待定
> - **HW03：** 截止时间：视教学进度待定
> - **Hola-Kola**个人案例作业：截止时间：视教学进度待定
> - **HW04：** 截止时间：视教学进度待定
> - **HW05：** 截止时间：视教学进度待定
---

**作业下载：**
- 链接：https://pan.baidu.com/s/1bldzlu_dDjNe6nqDrAah-w 提取码：j8d8

**作业提交方式：**
- (1) 无需打印，只需要提交电子版。
- (2) 所有作业均提交到 http://xzc.cn/gsST033o08. [【点击提交】](http://xzc.cn/gsST033o08)。
- (3) 请务必在电子文档中写上你的姓名和学号。
- (4) 截止时间：约定日期当日 23:59。



&emsp;

### 3. 案例分析报告 (小组团队完成)

> 前往 Wikis 页面查看：[案例分析展示具体要求](https://gitee.com/Dengjp/CF/wikis/CF%20%E6%A1%88%E4%BE%8B%E5%88%86%E6%9E%90%E6%8A%A5%E5%91%8A%E8%A6%81%E6%B1%82.md?sort_id=1635044)
- 估值专题：贵州茅台、哔哩哔哩
- 融资专题：顺丰、泰禾集团
- 股利专题：苹果、泸州老窖
- 收购和反收购专题：万科 
- 公司治理专题：阿里巴巴

**案例材料下载：**
- 链接：https://pan.baidu.com/s/1-EwzBCtIa1lMOJVonrjEDg 提取码：crqt

**案例展示PPT提交方式：**
- PPT提交到 http://xzc.cn/gsST033o08. [【点击提交】](http://xzc.cn/gsST033o08)。


&emsp;


### 4. 期末考试

- 考试时间：第 20 周
- 考试形式：中文、闭卷
- **说明：**
  1.	可以带一张 A4 小抄，正反面均可记录你认为有价值的信息；
  2.	可以带计算器（包括简易计算器和金融计算器）。


&emsp;

  
### 5. 教材和参考资料（附下载链接）
 
- Berk-DeMarzo，Corporate Finance - The Core，[[4th-E]](https://quqi.gblhgk.com/s/880197/BAK0oDvjWwNSxlha)
- Berk-DeMarzo，公司财务, 3th-中文([上](https://quqi.gblhgk.com/s/880197/1YUQk8Hr9iseBcJ5)，[下](https://quqi.gblhgk.com/s/880197/yAFra4o9dPfSjuzO))
- 达摩达兰-2015-4th-中文-应用公司理财，[[PDF]](https://quqi.gblhgk.com/s/880197/9HQqXTSS31U3X1cL)
- 达摩达兰-2015-4th-Applied Corporate Finance，[[PDF]](https://quqi.gblhgk.com/s/880197/Zy0ROq4Bv3xtGXKl)
- Asquith, P., Lawrence A., 2016，中文版，公司金融：金融工具、财务政策和估值方法的案例实践，[[PDF]](https://quqi.gblhgk.com/s/880197/P0MtVGIXaAFhsU5Q)
- Asquith, P., Lawrence A. Lessons in Corporate Finance. 2016. [[PDF]]()
- Holden-2015-Excel Modeling in Corporate Finance 5e, [[PDF]](https://quqi.gblhgk.com/s/880197/dBtrOjrbVQ4Ri9wP), [[Excel Tables]](https://quqi.gblhgk.com/s/880197/jHmEDaNQE7S5KPVL)

&emsp;

### 6. 附：课程内容和进度（计划）

周次 | 主题  | 学时  | 章节
---|:---|:---|:---|
第 1 周	| 公司财务简介 |	2	| 前言
第 2 周	| 公司组织形式和所有权结构 |	2	| 第1-2章
第 3 周	| 财务决策与一价定律 |	2	| 第3章
第 3 周	| 货币时间价值 |	2	| 第4章
第 4 周	| 利率理论 |	2	| 第5章
第 4 周	| 债券估值 |	2	| 第6章
第 5 周	| 投资决策法则 |	2	| 第7章
第 5 周	| 资本预算的基本原理 |	2	| 第8章
第 6 周	| 股票估值 |	2	| 第9章
第 7 周 |	资本市场与风险的定价 |	2	| 第10章
第 7 周 |	最优投资组合选择与资本资产定价模型 |	2	| 第11章
第 8 周 |	估计资本成本 |	2	| 第12章
第 9 周 |	完美市场中的资本结构 |	2	| 第14章
第 11-12 周 |	债务和税收 |	2	| 第15章
第 13-14 周 |	财务困境、管理者激励与信息 |	2	| 第16章
第 15 周 |	股利政策 |	2	| 第17章
第 16-17 周 |	兼并与收购 |	4	| 第28章
第 18-19 周 |	公司治理 |	4	| 第29章
第 20 周 |	期末考试 |	0	| 闭卷考试

> **温馨提示：** 第 1-9 周，每周两次课；第10周停课；第 11-19 周，每周一次课。